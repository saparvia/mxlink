'use strict';
// @license  magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&amp;dn=agpl-3.0.txt GPL-3.0

// Generate a link for the given mxid
function generate(mxid, clientside) {
	var hash = clientside ? '#/' : '';
	return window.location + hash + encodeURIComponent(mxid);
}

// Handle form submission in JS
var creation_form = document.getElementById('creation_form');
if(creation_form) {
	creation_form.addEventListener('submit', function(e) {
		var link = document.getElementById('generated-link');
		var mxid = document.getElementById('mxid').value;

		//var clientside = (window.location.protocol == 'file:');
		var clientside = true; // For now

		var href = generate(mxid, clientside);

		link.textContent = href;
		link.href = href;

		var share = document.getElementById('share-div');
		share.getElementsByClassName('var_mxid')[0].textContent = mxid;

		var sharelinks = document.getElementById('share-buttons').getElementsByTagName('a');
		for(var i = 0; i< sharelinks.length; i++) {
			sharelinks[i].href += encodeURIComponent(href); // FIXME: Need to store original / be able to restore it later somehow
		}

		share.style.display = 'block';

		document.getElementById('about').style.display = 'none'; // No need to clutter with about text anymore

		e.preventDefault(); // Don't actually submit the form to the server
	});
}

// @license-
