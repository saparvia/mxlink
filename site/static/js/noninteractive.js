'use strict';
// @license  magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&amp;dn=agpl-3.0.txt GPL-3.0

function isUserId(mxid) {
	return mxid[0] == '@';
}

function isRoomId(mxid) {
	return (mxid[0] == '#' || mxid[0] == '!');
}

function isMessageId(mxid) { // TODO: Is this actually correct?
	return (mxid[0] == '!' && mxid.indexOf('/$') > 0);
}

function isCommunityId(mxid) {
	return mxid[0] == '+';
}

var roomInfoDone = false;

function updateRoomInfo(mxid, access_token) {
	var parts = mxid.split(':');
	var server = parts[parts.length -1];

	updateElementsVisible('has_topic', false);
	updateElementsText('var_displayname', mxid);

	if(isUserId(mxid)) {
		createSpinner();
		getJSON(server, 'profile/' + encodeURIComponent(mxid) + '/displayname', access_token, function(result) {
			var name = result['displayname'];
			if(name) {
				updateElementsText('var_displayname', name + ' (' + mxid + ')');
			}
			else {
				updateElementsText('var_displayname', mxid);
			}
			sendRoomLoadFinished();
			removeSpinner();
		});
	}
	else if(isRoomId(mxid)) {
		createSpinner();
		postJSON(server, 'join/'+encodeURIComponent(mxid), {}, access_token, function(result) {
			var room_id = result['room_id'];
			getJSON(server, 'rooms/'+room_id+'/state/m.room.topic', access_token, function(result) {
				var topic = result['topic'];
				if(topic) {
					updateElementsText('var_roomtopic', topic);
					updateElementsVisible('has_topic', true);
				}
				getJSON(server, 'rooms/'+room_id+'/state/m.room.name', access_token, function(result) {
					var name = result['name'];
					if(name) {
						updateElementsText('var_displayname', name + ' (' + mxid + ')');
					}
					else {
						updateElementsText('var_displayname', mxid);
					}
					getJSON(server, 'rooms/'+room_id+'/state/m.room.encryption', access_token, function(result) {
						if(result['algorithm']) {
							updateElementsVisible('is_encrypted', true);
							updateElementsVisible('is_not_encrypted', false);
						}
						else {
							updateElementsVisible('is_encrypted', false);
							updateElementsVisible('is_not_encrypted', true);
						}

						sendRoomLoadFinished();

						removeSpinner();
						postJSON(server, 'rooms/'+room_id+'/leave', {}, access_token, function() {});
					});
				});
			});
		});
	}
	else if(isCommunityId(mxid)) {
		sendRoomLoadFinished();
	}
}

function sendRoomLoadFinished() {
	document.documentElement.dispatchEvent(new CustomEvent('roomInfoFinished', { bubbles: true, cancelable: false }));
}

function createSpinner() {
	var spinner = document.createElement('spinner');
	spinner.id = 'spinner';

	document.getElementsByTagName('body')[0].appendChild(spinner);
}

function removeSpinner() {
	var spinner = document.getElementById('spinner');
	spinner.parentNode.removeChild(spinner);
}

function updateLanguageLinks(hash) {
	var elems = document.getElementById('lang-selector').getElementsByTagName('a');
	for(var i=0; i < elems.length; i++) {
		var a = elems[i];
		var idx = a.href.indexOf('#');
		if(idx > 0) {
			a.href = a.href.substring(0, idx) + hash;
		}
		else {
			a.href = a.href + hash;
		}
	}
}

// Update whether to show the link generation or the instructions to join the room
function updateView() {
	var hash = decodeURIComponent(window.location.hash);
	if(hash) {
		updateLanguageLinks(hash);

		var mxid = hash.substring(2);

		// Is the address of any known type?
		if(!(isUserId(mxid) || isRoomId(mxid) || isMessageId(mxid) || isCommunityId(mxid))) {
			// TODO: Better error message
			window.alert('Invalid mxid ' + mxid);
			return;
		}

		// Strip away message ID so we can treat it as a room until needed
		var convo_index = mxid.indexOf('/$');
		var full_mxid = mxid;

		var parts = mxid.split(':');
		if(parts.length == 1) {
			// TODO: Better error handling
			window.alert('It looks like you provided an invalid Matrix identifier :-(');
			return;
		}

		var server = parts[parts.length -1];

		// Check to see if there is a link to a specific message
		if(convo_index > 0) {
			mxid = mxid.substring(0, convo_index);
		}

		updateElementsText('var_mxid', mxid);

		// Get information about room/user
		if(typeof localStorage !== 'undefined' && localStorage.access_token) {
			updateRoomInfo(mxid, localStorage.access_token);
		}
		else {
			// Need to register if we don't already have a auth_token
			postJSON(server, 'register?kind=guest', { 'password': String(Math.random()) }, '', function(result) {
				if(typeof localStorage !== 'undefined') localStorage.access_token = result['access_token'];
				updateRoomInfo(mxid, result['access_token']);
			});
		}

		document.getElementById('incoming').style.display = 'block';
		document.getElementById('outgoing').style.display = 'none';

		// Replace the client URLs with the appropriate one depending on the action to take
		var as = document.getElementById('client-selector').getElementsByTagName('a');
		for(var i in as) {
			var a = as[i];
			if(a.dataset) {
				if(isUserId(mxid) && a.dataset.user) {
					a.href = a.dataset.user.replace('{{mxid}}', mxid);
				}
				else if((isRoomId(mxid) || isMessageId(mxid)) && a.dataset.room) { // TODO: This works for now but might break later. Make separate for messages
					a.href = a.dataset.room.replace('{{mxid}}', full_mxid);
				} 
				else if((isCommunityId(mxid) && a.dataset.community)) {
					a.href = a.dataset.community.replace('{{mxid}}', mxid);
				} 
			}
		}

		// Show and hide the appropriate parts of instructions
		updateElementsVisible('is_user', isUserId(mxid));
		updateElementsVisible('is_room', isRoomId(mxid));
		updateElementsVisible('is_message', isMessageId(full_mxid));
		updateElementsVisible('is_community', isCommunityId(mxid));

		updateElementsVisible('is_not_user', !isUserId(mxid));
		updateElementsVisible('is_not_room', !isRoomId(mxid));
		updateElementsVisible('is_not_message', !isMessageId(full_mxid));
		updateElementsVisible('is_not_community', !isCommunityId(mxid));
	}
	else {
		document.getElementById('incoming').style.display = 'none';
		document.getElementById('outgoing').style.display = 'block';
	}
}

window.addEventListener('load', function() {
	updateView();
});
window.addEventListener('hashchange', function() {
	updateView();
});
window.addEventListener('roomInfoFinished', hideUnnecessaryHeaders);

function doJSON(server, url, method, data, access_token, callback) {
	url = 'https://' + server + '/_matrix/client/r0/' + url;
	if(access_token != '') {
		url += '?access_token='+access_token;
	}
	var http = new XMLHttpRequest();
	http.open(method, url, true);
	http.setRequestHeader('Content-type', 'application/json');
	http.onreadystatechange = function() {
		if(http.readyState === 4) {
			// TODO: Handle errors
			var response = http.responseText;
			callback(JSON.parse(response));
		}
	};
	http.send(JSON.stringify(data));
}

function postJSON(server, url, data, access_token, callback) {
	doJSON(server, url, 'POST', data, access_token, callback);
}

function getJSON(server, url, access_token, callback) {
	doJSON(server, url, 'GET', {}, access_token, callback);
}

function updateElementsText(className, text) {
	var elems = document.getElementsByClassName(className);
	for(var i = 0; i < elems.length; i++) {
		elems[i].textContent = text;
	}
}

function updateElementsVisible(className, visible) {
	var elems = document.getElementsByClassName(className);
	for(var i = 0; i < elems.length; i++) {
		// I hate CSS. Seriously. How hard does it need to be to toggle visibility
		var display = ['span'].indexOf(elems[i].tagName.toLowerCase()) == -1 ? 'block' : 'inline';
		elems[i].style.display = visible ? display : 'none';
	}
}

function hideUnnecessaryHeaders() {
	var sections = document.querySelectorAll('#client-selector > div');
	for(var i = 0; i < sections.length; i++) {
		var lis = sections[i].querySelectorAll('ul > li');
		var visible = 0;
		for(var j = 0; j < lis.length; j++) {
			if(lis[j].style.display != 'none') {
				visible += 1;
			}
		}
		// Nothing visible in this section, hide it
		if(visible == 0) {
			sections[i].style.display = 'none';
		}
	}
}

// @license-
