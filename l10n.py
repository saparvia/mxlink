import sys
import json
import xml.etree.ElementTree as ET

def stringify(elem):
    return "".join(
        ["" if elem.text is None else elem.text] + # Text content
        [ET.tostring(e, encoding='unicode') for e in list(elem)]) # The rest of the content

SRC_PATH = 'site/index.html'
LANG = sys.argv[1]
L10N_PATH = 'l10n/%s.json'%LANG

# Appending .html makes it easer to test.
# Remove when using server-side content negotiation and proper MIME types
OUT_PATH = '%s.%s.html'%(SRC_PATH, LANG)

def translate(tree):
    try:
        with open(L10N_PATH, encoding='utf-8') as infile:
            mapping = json.load(infile)
    except IOError:
        mapping = {}

    found_keys = set()

    # Find all elements that have a language. They should be translated.
    for elem in tree.findall('.//*[@lang]'):
        elem.attrib['lang'] = LANG
        # Loop over translatable attributes. None = content
        for attrib_name in [None, 'value', 'placeholder', 'x-moz-errormessage']:

            if attrib_name is None:
                # Stringify all the contents of the element
                string = str(stringify(elem).replace('\n', '').replace('\t', ''))
            else:
                try:
                    # Take the translatable string from the attribute
                    string = elem.attrib[attrib_name]
                except KeyError:
                    continue

            # Skip empty strings. They are not interesting
            if string == '':
                continue

            # Use the element id as key if it exists, otherwise just the string
            if 'id' in elem.attrib:
                # Use the element id + attribute name as key
                key = elem.attrib['id'] + ('-'+attrib_name if attrib_name != None else '')
            else:
                key = string

            found_keys.add(key)

            # Empty string means it is not translated
            if not key in mapping:
                mapping[key] = ''

            # If the key is in the mapping it means it must have been translated before. So use it.
            if mapping[key] != '':
                # Again, what is actually being translated?
                if attrib_name:
                    elem.attrib[attrib_name] = mapping[key]
                else:
                    # Remove the old content
                    for child in list(elem):
                        elem.remove(child)

                    # I don't know how to parse and insert an XML fragment, so let's do it like this
                    xml_text = '<root>%s</root>'%mapping[key] # Make sure it is well-formed
                    temp = ET.fromstring(xml_text)
                    elem.text = temp.text
                    for child in list(temp):
                        elem.append(child)
                    elem.tail = temp.tail

    for key in mapping.keys()-found_keys:
        del mapping[key]

    return mapping

def main():
    tree = ET.parse(SRC_PATH)
    mapping = translate(tree)
    outfile = open(L10N_PATH, 'w', encoding='utf-8')
    json.dump(mapping, outfile, indent=2, ensure_ascii=False)
    tree.write(OUT_PATH, method='html')

if __name__ == '__main__':
    main()
